# Jaguar-Toystore

## Tech stack:

* Spring boot
* Hibernate
* DB: H2
* MapStruct

## Showcase

* MapStruct mapping: Entity -> Dto
* Connect to H2 db
* Entity graph
