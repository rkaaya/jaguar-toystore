INSERT INTO TOY (id, name) VALUES (101, 'Car'),
                                  (102, 'Bear') ON CONFLICT (id) DO NOTHING;

INSERT INTO COLOUR (id, name) VALUES (101, 'Green'),
                                     (102, 'Orange') ON CONFLICT (id) DO NOTHING;

INSERT INTO MATERIAL (id, name, toy_id) VALUES (101, 'Steel', 101),
                                               (102, 'Wooden', 101) ON CONFLICT (id) DO NOTHING;

INSERT INTO TOY_COLOUR (TOY_ID, COLOUR_ID) VALUES (101, 101),
                                                  (101, 102) ON CONFLICT (id) DO NOTHING;