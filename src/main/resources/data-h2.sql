MERGE INTO TOY (id, name) VALUES (101, 'Car'),
                                  (102, 'Bear');

MERGE INTO COLOUR (id, name) VALUES (101, 'Green'),
                                     (102, 'Orange');

MERGE INTO MATERIAL (id, name, toy_id) VALUES (101, 'Steel', 101),
                                               (102, 'Wooden', 101);

MERGE INTO TOY_COLOUR (TOY_ID, COLOUR_ID) VALUES (101, 101),
                                                  (101, 102);

MERGE INTO LOCATION (id, location_name) VALUES (100, 'Forrest');

MERGE INTO POKEMON (pokename, type, power, location) VALUES ('Pokibu', 'Earth', 101, 100);

MERGE INTO DIGIMON (diginame, type, power, location) VALUES ('Dikobu', 'Earth', 101, 100);