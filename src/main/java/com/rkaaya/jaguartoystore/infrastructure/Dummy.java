package com.rkaaya.jaguartoystore.infrastructure;

import com.querydsl.core.types.Predicate;
import com.rkaaya.jaguartoystore.api.models.ToyDTO;
import com.rkaaya.jaguartoystore.api.services.ToyService;
import com.rkaaya.jaguartoystore.services.entities.*;
import com.rkaaya.jaguartoystore.services.mapper.CycleAvoidingMappingContext;
import com.rkaaya.jaguartoystore.services.mapper.MapStructMapper;
import com.rkaaya.jaguartoystore.services.repositories.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@Slf4j
public class Dummy {

    private ToyService toyService;

    private DigimonRepository digimonRepository;
    private PokemonRepository pokemonRepository;
    private LocationRepository locationRepository;
    private CityRepository cityRepository;

    private MapStructMapper mapStructMapper;

    @GetMapping(
            value = "/toys",
            produces = { "application/json" }
    )
    public ResponseEntity<ToyDTO> getToys() {
        ToyEntity toyEntity = toyService.getToys();
        ToyDTO toyDTO = mapStructMapper.toyToToyDto(toyEntity, new CycleAvoidingMappingContext());
        return ResponseEntity.ok(toyDTO);
    }

    @GetMapping(
            value = "/city",
            produces = { "application/json" }
    )
    public ResponseEntity<CityEntity> getCity() {
        QCityEntity qCityEntity = QCityEntity.cityEntity;
        Predicate predicate = qCityEntity.population.between(1,2);
        Optional<CityEntity> cityEntity = cityRepository.findOne(predicate);
        return ResponseEntity.ok(cityEntity.get());
    }

    @GetMapping(
            value = "/creatures",
            produces = { "application/json" }
    )
    public ResponseEntity<Void> getCreatures() {
        log.info("Digimons:");
        List<DigimonEntity> digimons = digimonRepository.findAll();

        log.info("Pokemons");
        List<PokemonEntity> pokemons = pokemonRepository.findAll();

        log.info("Locations:");
        List<LocationEntity> locations = locationRepository.findAll();

        PokemonEntity pe = new PokemonEntity();
        PokemonEntity.PokemonPk pek = PokemonEntity.PokemonPk.builder()
                .name("poke")
                .type("fory")
                .location(locations.get(0))
                .build();
        pe.setPrimaryKey(pek);
        pokemonRepository.save(pe);

        DigimonEntity de = new DigimonEntity();
        de.setName("z");
        de.setPower(123);
        de.setType("x");
        de.setLocation(locations.get(0));
        digimonRepository.save(de);

        log.info("Digimons:");
        List<DigimonEntity> digimons2 = digimonRepository.findAll();

        return ResponseEntity.ok().build();
    }

    @GetMapping(
            value = "/toys/{toyId}",
            produces = { "application/json" }
    )
    public ResponseEntity<ToyDTO> getToys(@PathVariable("toyId") Integer toyId) {
        log.info("enter: {}", toyId);
        ToyEntity toyEntity = toyService.getToys(toyId);
        ToyDTO toyDTO = mapStructMapper.toyToToyDto(toyEntity, new CycleAvoidingMappingContext());
        return ResponseEntity.ok(toyDTO);
    }
}
