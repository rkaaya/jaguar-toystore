package com.rkaaya.jaguartoystore.configuration;

import com.rkaaya.jaguartoystore.api.services.ToyService;
import com.rkaaya.jaguartoystore.services.DefaultToyService;
import com.rkaaya.jaguartoystore.services.mapper.MapStructMapper;
import com.rkaaya.jaguartoystore.services.mapper.MapStructMapperImpl;
import com.rkaaya.jaguartoystore.services.repositories.ToyRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ToyConfiguration {

    @Bean
    public ToyService toyService(final ToyRepository toyRepository) {
        return new DefaultToyService(toyRepository);
    }

    @Bean
    public MapStructMapper mapStructMapper(){
        return new MapStructMapperImpl();
    }
}
