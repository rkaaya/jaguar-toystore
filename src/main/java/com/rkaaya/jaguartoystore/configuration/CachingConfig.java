package com.rkaaya.jaguartoystore.configuration;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CachingConfig {

    @Bean
    public Caffeine caffeineConfig() {
        return Caffeine.newBuilder().expireAfterWrite(30, TimeUnit.SECONDS);
    }

    //Config for all
    @Bean
    public CacheManager cacheManager(Caffeine caffeine) {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        caffeineCacheManager.setCaffeine(caffeine);
        return caffeineCacheManager;
    }

    //Separate Config
//    @Bean
//    public CacheManager cacheManager(Caffeine caffeine) {
//        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
//        simpleCacheManager.setCaches(List.of(
//                cacheToy()
//        ));
//        return simpleCacheManager;
//    }

    @Bean
    public CaffeineCache cacheToy() {
        return new CaffeineCache("toys rnd",
                Caffeine.newBuilder()
                        .expireAfterAccess(5, TimeUnit.SECONDS)
                        .build());
    }
}
