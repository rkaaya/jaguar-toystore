package com.rkaaya.jaguartoystore.api.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ToyDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("colours")
    @JsonManagedReference
    private Set<ColourDTO> colourEntities;

    @JsonProperty("materials")
    @JsonManagedReference
    private Set<MaterialDTO> materialEntities;

    @JsonProperty("parts")
    @JsonManagedReference
    private Set<String> parts;
}
