package com.rkaaya.jaguartoystore.api.services;

import com.rkaaya.jaguartoystore.services.entities.ToyEntity;

public interface ToyService {
     ToyEntity getToys();
     ToyEntity getToys(Integer rnd);
}
