package com.rkaaya.jaguartoystore.runnable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan("com.rkaaya.jaguartoystore.services.entities")
@EnableJpaRepositories(basePackages = {"com.rkaaya.jaguartoystore.services.repositories"})
@SpringBootApplication(scanBasePackages = {"com.rkaaya.jaguartoystore.configuration", "com.rkaaya.jaguartoystore.infrastructure"})
public class JaguarToystoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(JaguarToystoreApplication.class, args);
    }

}
