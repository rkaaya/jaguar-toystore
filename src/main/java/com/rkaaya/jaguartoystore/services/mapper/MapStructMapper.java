package com.rkaaya.jaguartoystore.services.mapper;

import com.rkaaya.jaguartoystore.api.models.ColourDTO;
import com.rkaaya.jaguartoystore.api.models.MaterialDTO;
import com.rkaaya.jaguartoystore.api.models.ToyDTO;
import com.rkaaya.jaguartoystore.services.entities.ColourEntity;
import com.rkaaya.jaguartoystore.services.entities.MaterialEntity;
import com.rkaaya.jaguartoystore.services.entities.ToyEntity;
import org.mapstruct.Context;
import org.mapstruct.Mapper;

@Mapper
public interface MapStructMapper {

    ColourDTO colourToColourDto(ColourEntity colour, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);

    MaterialDTO materialToMaterialDto(MaterialEntity material, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);

    ToyDTO toyToToyDto(ToyEntity toy, @Context CycleAvoidingMappingContext cycleAvoidingMappingContext);
}
