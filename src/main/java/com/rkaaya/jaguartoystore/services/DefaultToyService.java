package com.rkaaya.jaguartoystore.services;

import com.rkaaya.jaguartoystore.api.services.ToyService;
import com.rkaaya.jaguartoystore.services.entities.ToyEntity;
import com.rkaaya.jaguartoystore.services.repositories.ToyRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;

import java.util.Set;

@AllArgsConstructor
@Slf4j
public class DefaultToyService implements ToyService {

    private ToyRepository toyRepository;

    @Cacheable("toys")
    @Override
    public ToyEntity getToys() {
        log.info("was here");
        ToyEntity toy = new ToyEntity();
        toy.setId(5L);
        toy.setName("Wux");
        toy.setParts(Set.of("asd", "zzz", "ggg", "zxcccc"));
        toyRepository.save(toy);
        ToyEntity toy2 = new ToyEntity();
        toy2.setId(5L);
        toy2.setName("Wuxa");
        toy2.setParts(Set.of("asdz", "zzza", "ggg1", "zxcccc2"));
        toyRepository.save(toy2);
        return toyRepository.findByName("Wux");
    }

    @Cacheable("toys rnd")
    @Override
    public ToyEntity getToys(Integer rnd) {
        log.info("was here: {}", rnd);
        ToyEntity toy = new ToyEntity();
        toy.setId(5L);
        toy.setName("Wux");
        toy.setParts(Set.of("asd", "zzz", "ggg", "zxcccc"));
        return toyRepository.save(toy);
    }
}
