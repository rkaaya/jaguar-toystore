package com.rkaaya.jaguartoystore.services.repositories;

import com.rkaaya.jaguartoystore.services.entities.PokemonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonRepository extends JpaRepository<PokemonEntity, PokemonEntity.PokemonPk> {

}
