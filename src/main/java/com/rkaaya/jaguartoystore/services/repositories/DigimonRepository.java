package com.rkaaya.jaguartoystore.services.repositories;

import com.rkaaya.jaguartoystore.services.entities.DigimonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DigimonRepository extends JpaRepository<DigimonEntity, DigimonEntity.DigimonPk> {

}
