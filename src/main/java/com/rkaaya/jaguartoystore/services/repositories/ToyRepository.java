package com.rkaaya.jaguartoystore.services.repositories;

import com.rkaaya.jaguartoystore.services.entities.ToyEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToyRepository extends JpaRepository<ToyEntity, Long> {

    @EntityGraph(value = "Toy.thingy")
    ToyEntity findByName(String name);
}
