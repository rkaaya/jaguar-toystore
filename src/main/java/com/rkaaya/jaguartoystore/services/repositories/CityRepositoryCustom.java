package com.rkaaya.jaguartoystore.services.repositories;

import com.rkaaya.jaguartoystore.services.entities.CityEntity;

public interface CityRepositoryCustom {
    Iterable<CityEntity> findAllBetweenPopulation(Integer from, Integer to);
}
