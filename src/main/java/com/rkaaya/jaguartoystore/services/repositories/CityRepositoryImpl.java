package com.rkaaya.jaguartoystore.services.repositories;

import com.rkaaya.jaguartoystore.services.entities.CityEntity;
import com.rkaaya.jaguartoystore.services.entities.QCityEntity;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

public class CityRepositoryImpl extends QuerydslRepositorySupport implements CityRepositoryCustom {

    public CityRepositoryImpl(Class<?> domainClass) {
        super(domainClass);
    }

    @Override
    public Iterable<CityEntity> findAllBetweenPopulation(Integer from, Integer to) {
        QCityEntity city = QCityEntity.cityEntity;
        return from(city)
                .where(city.population.between(from, to))
                .fetch();
    }
}
