package com.rkaaya.jaguartoystore.services.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "POKEMON")
public class PokemonEntity {

    @Getter
    @Setter
    @Builder
    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PokemonPk implements Serializable {

        @Column(name = "TYPE")
        private String type;

        @Column(name = "POKENAME")
        private String name;

        @ManyToOne
        @JoinColumn(name="location", nullable=false)
        private LocationEntity location;
    }

    @EmbeddedId
    private PokemonPk primaryKey;

    @Basic
    private Integer power;
}
