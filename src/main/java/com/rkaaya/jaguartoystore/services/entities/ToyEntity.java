package com.rkaaya.jaguartoystore.services.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "Toy")
@NamedEntityGraph(name = "Toy.thingy",
    attributeNodes = {
        @NamedAttributeNode("materialEntities"),
        @NamedAttributeNode("colourEntities")
    }
)
public class ToyEntity {

    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @ManyToMany
    @JoinTable(
            name = "toy_colour",
            joinColumns = @JoinColumn(name = "toy_id"),
            inverseJoinColumns = @JoinColumn(name = "colour_id"))
    private Set<ColourEntity> colourEntities;

    @OneToMany(mappedBy = "toy")
    private Set<MaterialEntity> materialEntities;

    @ElementCollection
    @CollectionTable(name = "TOY_PARTS")
    private Set<String> parts;
}
