package com.rkaaya.jaguartoystore.services.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Material")
public class MaterialEntity {

    @Id
    @Basic
    @GeneratedValue
    private Long id;

    @Basic
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private ToyEntity toy;

}
