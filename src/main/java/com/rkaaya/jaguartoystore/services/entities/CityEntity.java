package com.rkaaya.jaguartoystore.services.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "City")
public class CityEntity {

    @Id
    @Column
    @GeneratedValue
    private Long id;

    @Basic
    private String name;

    @Basic
    private Integer population;

}
