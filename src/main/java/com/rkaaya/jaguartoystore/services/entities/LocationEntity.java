package com.rkaaya.jaguartoystore.services.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "LOCATION")
public class LocationEntity {

    @Id
    @Basic
    @GeneratedValue
    private Long id;

    @Column(name = "LOCATION_NAME")
    private String name;

    @OneToMany(mappedBy="primaryKey.location")
    private Set<PokemonEntity> pokemons;

    @OneToMany(mappedBy="location")
    private Set<DigimonEntity> digimons;
}
