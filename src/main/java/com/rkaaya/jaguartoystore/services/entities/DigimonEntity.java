package com.rkaaya.jaguartoystore.services.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DIGIMON")
@IdClass(DigimonEntity.DigimonPk.class)
@Entity
public class DigimonEntity {

    @Embeddable
    public static class DigimonPk implements Serializable {
        private String type;
        private String name;
        private LocationEntity location;
    }

    @Id
    @Column(name = "TYPE")
    private String type;

    @Id
    @Column(name = "DIGINAME")
    private String name;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="location")
    private LocationEntity location;
    
    @Basic
    private Integer power;
}
