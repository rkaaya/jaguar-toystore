package com.rkaaya.jaguartoystore.services.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "Colour")
public class ColourEntity {

    @Id
    @Basic
    @GeneratedValue
    private Long id;

    @Basic
    private String name;

    @ManyToMany(mappedBy = "colourEntities")
    Set<ToyEntity> toyEntities;
}
